﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Sbk_Manage
{
    public partial class Frm_HtChance : Form
    {
        public string Ht_Code = "";
        public int Ht_Id = 0;
        public Frm_HtChance()
        {
            InitializeComponent();
            this.KeyPreview = true;
        }
        public void KeyDownProcess(KeyEventArgs e, int btn_type, string strValue)
        {
            if (e.KeyCode == Keys.Up || e.KeyCode == Keys.Down)
            {
                e.Handled = true;
                if (this.dataGridView1.SelectedRows.Count == 0)
                {
                    if (this.dataGridView1.Rows.Count > 0)
                    {
                        //先清空以前选中的所有行
                        dataGridView1.ClearSelection();
                        //最前面的黑色三角号跟着移动
                        this.dataGridView1.FirstDisplayedScrollingRowIndex = 0;
                        //选中第一行
                        dataGridView1.CurrentCell = dataGridView1.Rows[0].Cells["htbh"];
                        this.dataGridView1.Rows[0].Selected = true;
                       
                    }
                }
                else
                {
                    int iIndex = this.dataGridView1.SelectedRows[0].Index;
                    if (e.KeyCode == Keys.Up)
                    {
                        if (iIndex >= 1)
                        {
                            //先清空以前选中的所有行
                            dataGridView1.ClearSelection();
                            //最前面的黑色三角号跟着移动
                            this.dataGridView1.FirstDisplayedScrollingRowIndex = iIndex-1;
                            //选中
                            dataGridView1.CurrentCell = dataGridView1.Rows[iIndex - 1].Cells["htbh"];
                            this.dataGridView1.Rows[iIndex - 1].Selected = true;
                        }
                    }
                    else if (e.KeyCode == Keys.Down)
                    {
                        if (iIndex <= this.dataGridView1.Rows.Count - 2)
                        {
                            //先清空以前选中的所有行
                            dataGridView1.ClearSelection();
                            //最前面的黑色三角号跟着移动
                            this.dataGridView1.FirstDisplayedScrollingRowIndex = iIndex + 1;
                            //选中
                            dataGridView1.CurrentCell = dataGridView1.Rows[iIndex + 1].Cells["htbh"];
                            this.dataGridView1.Rows[iIndex + 1].Selected = true;
                        }
                    }
                }

            }
            if (e.KeyCode == Keys.Enter)
            {
                if (this.dataGridView1.Rows.Count > 0)
                {
                    int iIndex = this.dataGridView1.SelectedRows[0].Index;
                    Ht_Code = dataGridView1.Rows[iIndex].Cells["htbh"].Value.ToString();
                    Ht_Id =Convert.ToInt32(dataGridView1.Rows[iIndex].Cells["id"].Value.ToString());
                    DialogResult = DialogResult.OK;
                }
            }
        }
        private void Frm_HtChance_Load(object sender, EventArgs e)
        {
            //设置datagridview的列宽自动适应,列不可排序
            for (int i = 0; i < dataGridView1.ColumnCount ; i++)
            {
                dataGridView1.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            }
            //查询当年的合同  
            string sqlstr = "select id,htbh,xmbh,zbdlgs,htmc,ghs,hte,strftime('%Y-%m-%d',htqdrq) as htqdrq,strftime('%Y-%m-%d',htysrq) as htysrq,ghskpxx,strftime('%Y-%m-%d %H:%M:%S',htlrrq) as htlrrq,htlrr,scan_flag,jc_flag from ht_info where del_flag=0  and  strftime('%Y', htlrrq) ='" + DateTime.Now.ToString("yyyy") + "' order by id desc";
            this.dataGridView1.DataSource = Program.SqliteDB.GetDataTable(sqlstr);
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            KeyDownProcess(e, 1, "");
        }

        private void textBox2_KeyDown(object sender, KeyEventArgs e)
        {
            KeyDownProcess(e, 2, "");
        }

        private void textBox3_KeyDown(object sender, KeyEventArgs e)
        {
            KeyDownProcess(e, 3, "");
        }

        private void dataGridView1_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if( e.RowIndex == -1 )
            {
                return;
            }
            if(this.dataGridView1.RowCount > 0 && dataGridView1.SelectedRows.Count == 1 )
            {
                //
                Ht_Code = dataGridView1.SelectedRows[0].Cells["htbh"].Value.ToString();
                Ht_Id = Convert.ToInt32(dataGridView1.Rows[0].Cells["id"].Value.ToString());
                DialogResult = DialogResult.OK;
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
             dataGridView1.DataSource = null;
             if (!string.IsNullOrEmpty(this.textBox1.Text.Trim()))
             {
                 //按照合同拼音码查询  
                 string sqlstr = "select id,htbh,xmbh,zbdlgs,htmc,ghs,hte,strftime('%Y-%m-%d',htqdrq) as htqdrq,strftime('%Y-%m-%d',htysrq) as htysrq,ghskpxx,strftime('%Y-%m-%d %H:%M:%S',htlrrq) as htlrrq,htlrr,scan_flag,jc_flag from ht_info where del_flag=0  and htpy like '%" + textBox1.Text.Trim().Replace("'", "").ToUpper() + "%' order by id desc";
                 this.dataGridView1.DataSource = Program.SqliteDB.GetDataTable(sqlstr);
             }
             else
             {
                 //查询当年的合同  
                 string sqlstr = "select id,htbh,xmbh,zbdlgs,htmc,ghs,hte,strftime('%Y-%m-%d',htqdrq) as htqdrq,strftime('%Y-%m-%d',htysrq) as htysrq,ghskpxx,strftime('%Y-%m-%d %H:%M:%S',htlrrq) as htlrrq,htlrr,scan_flag,jc_flag from ht_info where del_flag=0  and  strftime('%Y', htlrrq) ='" + DateTime.Now.ToString("yyyy") + "' order by id desc";
                 this.dataGridView1.DataSource = Program.SqliteDB.GetDataTable(sqlstr);
             }
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }
    }
}
