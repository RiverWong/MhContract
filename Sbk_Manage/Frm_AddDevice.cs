﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Sbk_Manage
{
    public partial class Frm_AddDevice : Form
    {
        public Frm_AddDevice()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //验收日期 入账日期
            string ysrq = "";
            string rzrq = "";
            int Itf = 0;
            int Ijk = 0;
            string xcjcrq = "";
            string bfrq = "";
            int ibf=0;
            if (txtHtbh.Text.Trim().Equals(""))
            {
                MessageBox.Show("合同编号不能为空！");
                txtHtbh.Focus();
                return;
            }
            if(txtSbmc.Text.Trim().Equals(""))
            {
                MessageBox.Show("设备名称不能为空！");
                txtSbmc.Focus();
                return;
            }
            if (chkJk.Checked)
            {
                if (txtWmgsmc.Text.Trim().Equals("") || this.txtBgd.Text.Trim().Equals("") || this.txtJyjyzm.Text.Trim().Equals(""))
                {
                    MessageBox.Show("进口设备【外贸公司、报关单、检验检疫】三个都不能为空值！");
                    return;
                }
            }
            if (chkBfwh.Checked)
            {
                if (txtBfwh.Text.Trim().Equals(""))
                {
                    MessageBox.Show("报废文号不能为空值！");
                    return;
                }
                ibf=1;
                bfrq = this.dtpBfrq.Value.ToString("yyyy-MM-dd");
            }
            //
            if(cmbSblb.Text.Equals("计量") || cmbSblb.Text.Equals("特种"))
            {
                if (dtpjyrq.Value.ToString("yyyy-MM-dd").Equals(DateTime.Now.ToString("yyyy-MM-dd")))
                {
                    MessageBox.Show("下次检测日期不能为当天！");
                    return;
                }
                xcjcrq = dtpjyrq.Value.ToString("yyyy-MM-dd");
            }
            else if (cmbSblb.Text.Equals("射线装置"))
            {
                if (textSxzz.Text.Trim().Equals(""))
                {
                    MessageBox.Show("射线装置职业病预控评不能为空值！");
                    return;
                }
            }
            if (chkJk.Checked)
            {
                Ijk = 1;
            }
            if (chkTf.Checked)
            {
                Itf = 1;
            }
            if (chkYsrq.Checked)
            {
                ysrq = this.dtpYsrq.Value.ToString("yyyy-MM-dd");
            }
            if (chkRzrq.Checked)
            {
                rzrq = this.dtpRzrq.Value.ToString("yyyy-MM-dd");
            }

            string sqlstr = string.Format("insert into sb_info values(NULL,'{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}','{15}','{16}','{17}','{18}','{19}','{20}','{21}','{22}','{23}','{24}','{25}','{26}','{27}','{28}','{29}')", this.txtSbmc.Text.Trim().Replace("'", ""), PinYinConverter.GetFirst(this.txtSbmc.Text.Trim().Replace("'", "")), this.txtXhgg.Text.Trim().Replace("'", ""), this.txtCcbh.Text.Trim().Replace("'", ""), this.txtSbtm.Text.Trim().Replace("'", ""), this.txtSbpp.Text.Trim().Replace("'", ""), this.txtZczh.Text.Trim().Replace("'", ""), this.txtSyks.Text.Trim().Replace("'", ""), this.txtGhs.Text.Trim().Replace("'", ""), this.txtGhslxfs.Text.Trim().Replace("'", ""), this.txtCj.Text.Trim().Replace("'", ""), this.txtCjlxfs.Text.Trim().Replace("'", ""), ysrq, rzrq, this.txtZbq.Text.Trim().Replace("'", ""), txtSbsl.Text.Trim(), txtSbdj.Text.Trim(), this.txtSbbzsm.Text.Trim().Replace("'", ""), cmbSblb.Text, Itf, Ijk, this.txtWmgsmc.Text.Trim(), this.txtBgd.Text.Trim(), this.txtJyjyzm.Text.Trim().Replace("'", ""), xcjcrq, this.textSxzz.Text.Trim().Replace("'", ""), ibf, bfrq, this.txtBfwh.Text.Trim().Replace("'", ""), this.txtHtbh.Text.Trim().Replace("'", ""));
            if (Program.SqliteDB.ExecuteNonQuery(sqlstr) == 1)
            {
                MessageBox.Show("设备添加成功!", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                DialogResult = DialogResult.OK;
            }

        }
        private void txtSbdj_LostFocus(object sender, EventArgs e)
        {
            if (!txtSbdj.Text.Trim().Equals(""))
            {
                txtSbdj.Text = ToDBC(txtSbdj.Text);
                if (Microsoft.VisualBasic.Information.IsNumeric(txtSbdj.Text.Trim()) == false)
                {
                    txtSbdj.Text = "";
                    txtSbdj.Focus();
                }
            }
            else
            {
                txtSbdj.Text = "0";
            }
        }
        private void txtSbsl_LostFocus(object sender, EventArgs e)
        {
            if (!txtSbsl.Text.Trim().Equals(""))
            {
                txtSbsl.Text = ToDBC(txtSbsl.Text);
                if (Microsoft.VisualBasic.Information.IsNumeric(txtSbsl.Text.Trim()) == false)
                {
                    txtSbsl.Text = "";
                    txtSbsl.Focus();
                }
            }
            else
            {
                txtSbsl.Text = "";
            }
        }
        ///转半角的函数(DBC case)
        ///全角空格为12288，半角空格为32
        ///其他字符半角(33-126)与全角(65281-65374)的对应关系是：均相差65248// 
        public static string ToDBC(string input)
        {
            char[] array = input.ToCharArray();
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] == 12288)
                {
                    array[i] = (char)32;
                    continue;
                }
                if (array[i] > 65280 && array[i] < 65375)
                {
                    array[i] = (char)(array[i] - 65248);
                }
            }
            return new string(array);
        }
        //合同检索
        private void button1_Click(object sender, EventArgs e)
        {
            Frm_HtChance ins = new Frm_HtChance();
            if (ins.ShowDialog() == DialogResult.OK)
            {
                txtHtbh.Text = ins.Ht_Code;
                int ht_id = ins.Ht_Id;
                string sqlstr = "select id,htbh,xmbh,zbdlgs,htmc,ghs,hte,strftime('%Y-%m-%d',htqdrq) as htqdrq,strftime('%Y-%m-%d',htysrq) as htysrq,ghskpxx,strftime('%Y-%m-%d %H:%M:%S',htlrrq) as htlrrq,htlrr from ht_info where del_flag=0 and id=" + ht_id;
                DataTable dt = Program.SqliteDB.GetDataTable(sqlstr);
                if (dt.Rows.Count > 0)
                {
                    txtGhs.Text = dt.Rows[0]["ghs"].ToString();
                    if (dt.Rows[0]["htysrq"].ToString().Equals(""))
                    {
                        this.chkYsrq.Checked = false;
                    }
                    else
                    {
                        chkYsrq.Checked = true;
                        this.dtpYsrq.Value = Convert.ToDateTime(dt.Rows[0]["htysrq"].ToString());
                    }
                }
                txtSbmc.Focus();
            }
        }

        private void cmbSblb_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void Frm_AddDevice_Load(object sender, EventArgs e)
        {
            cmbSblb.SelectedIndex = 0;
            this.txtSbdj.LostFocus += new EventHandler(txtSbdj_LostFocus);
            this.txtSbsl.LostFocus += new EventHandler(txtSbsl_LostFocus);
        }

        
    }
}
